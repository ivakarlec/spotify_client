"use strict";
const express = require("express");
const path = require("path");
const request = require("request");
const querystring = require("querystring");

const spotifyConfig = require("./spotify.config.json");

//const host = "http://localhost"
const SERVER_PORT = process.env.PORT || "4040";
const CLIENT_PORT = process.env.NODE_ENV === "production" ? SERVER_PORT : process.env.DEV_PORT;
const clientID = spotifyConfig.clientID;
const clientSecret = spotifyConfig.clientSecret;
const redirectUri = `http://localhost:${SERVER_PORT}/callback`;

const app = express();
const bodyParser = require("body-parser");
app.use( bodyParser.json() );

let auth = {
    isAuthenticated: false,
    name: ""
};

function requestSpotifyApi(url, params, res) {
    const searchOptions = {
        url: url,
        headers: { "Authorization": "Bearer " + auth.access_token },
        json: true,
        qs: params
    };
    request.get(searchOptions, (error, response, data) => {
        if(error) {
            console.error(error);
            res.send(error);
        } else if (!data) {
            console.error(data);
            res.send({
                error: {
                    message: "No data received from the spotify api."
                }
            });
        } else if (data.hasOwnProperty("error") && data.error.status === 401){
            auth = {
                isAuthenticated: false,
                name: ""
            };
            res.send({
                isAuthenticated: auth.isAuthenticated,
                authMessage: data.error.message
            });
        } else {
            res.send(data);
        }
    });
}

app.post("/api/spotifySearch", (req, res) => {
    requestSpotifyApi("https://api.spotify.com/v1/search", {
        q: req.body.q,
        type: ["album", "artist", "playlist", "track"].join(",")
    }, res);
});

app.get("/api/checkAuthentication.json", (req, res) => {
    res.json({
        isAuthenticated: auth.isAuthenticated,
        userName: auth.name
    });
});

app.get("/api/logout", (req, res) => {
    auth = {
        isAuthenticated: false,
        name: ""
    };
    res.sendStatus(200);
});

app.get("/callback", (req, res) => {
    // your application requests refresh and access tokens
    // after checking the state parameter

    const code = req.query.code || null;
    var authOptions = {
        url: "https://accounts.spotify.com/api/token",
        form: {
            code: code,
            redirect_uri: redirectUri,
            grant_type: "authorization_code"
        },
        headers: {
            "Authorization": "Basic " + (new Buffer(clientID + ":" + clientSecret).toString("base64"))
        },
        json: true
    };

    request.post(authOptions, (error, response, body) => {
        if (!error && response.statusCode === 200) {

            auth.isAuthenticated = true;
            auth.access_token = body.access_token;
            auth.refresh_token = body.refresh_token;

            var options = {
                url: "https://api.spotify.com/v1/me",
                headers: { "Authorization": "Bearer " + auth.access_token },
                json: true
            };

            // use the access token to access the Spotify Web API
            request.get(options, (error, response, body) => {
                auth.name = body.display_name;
            });
            // we can also pass the token to the browser to make requests from there
            res.redirect(`http://localhost:${CLIENT_PORT}`);
        } else {
            res.redirect("/login" +
                querystring.stringify({
                    error: "invalid_token"
                })
            );
        }
    });

});

app.use(express.static(path.join(__dirname, "dist")));
app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname + "/dist/index.html"));
});

let server = app.listen(SERVER_PORT, function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log("This app is listening at http://", host, port);
});
