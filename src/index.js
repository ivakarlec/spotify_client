import "core-js/es6/map";
import "core-js/es6/set";
import "core-js/es6/promise";

import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "./index.scss";

ReactDOM.render(<App/>, document.getElementById("root"));
