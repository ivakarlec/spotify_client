import React, {Component} from "react";
import {Route, Redirect} from "react-router-dom";
import Axios from "axios";

import LoginNav from "./SpotifyLogin/NavBar";
import Loader from "./Loader";

class PrivateRoute extends Component {
    componentDidMount() {
        this.checkAuthentication()
            .then((isAuthenticated) => {
                this.props.onAuthChange({
                    loading: false,
                    ...isAuthenticated
                });
            }).catch(err => {
                console.error(err);
                this.props.onAuthChange({
                    loading: false,
                    authMessage: "Authorization for this Client failed."
                });
            });
    }
    checkAuthentication() {
        return new Promise((resolve, reject) => {
            Axios.get("/api/checkAuthentication.json")
                .then(response => resolve(response.data))
                .catch(error => reject(error));
        });
    }
    render() {
        const { component: Component, loading, isAuthenticated, ...rest } = this.props;
        if (loading) {
            return (<Loader/>);
        } else {
            return (
                <Route {...rest} render={ () => (
                    <div className="page-content">
                        {!isAuthenticated &&
                            <Redirect to={{ pathname: "/login", state: { from: this.props.location, errorMessage: this.props.authError } }} />
                        }
                        <LoginNav {...rest}/>
                        <Component {...rest} />
                    </div>
                )}
                />
            );
        }
    }
}
export default PrivateRoute;
