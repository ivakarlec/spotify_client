/**
 * Reusable Modal Component
 */
import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import "./Modal.scss";

class Modal extends Component {
    render() {
        const { show, bg } = this.props;
        // Custom styles: set visibility and backbround color
        const styles = {
            modal: {
                display: (show) ? null : "none",
                backgroundColor: bg || "rgba(255, 255, 255, 0.9)",
            }
        };
        const contentClassName = ["modal-content", this.props.contentClassName].join(" ");
        return ReactDOM.createPortal(
            <div className="modal"
                style={styles.modal}
                onClick={(e)=>{
                    if (e.target.getAttribute("class") === "modal") {
                        this.props.onClose();
                    }
                }}>
                <div className={contentClassName}>
                    { /* Close Button: invoke callback */ }
                    <span className="close" onClick={this.props.onClose} >x</span>
                    { /* Content */ }
                    { this.props.children }
                </div>
            </div>
            , document.getElementById("modal"));
    }
}
Modal.propTypes = {
    onClose: PropTypes.func.isRequired,
    show: PropTypes.bool,
    children: PropTypes.element,
    contentClassName: PropTypes.string
};
Modal.defaultProps = {
    show: false,
    contentClassName: ""
};
export default Modal;
