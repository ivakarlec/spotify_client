import Axios from "axios";

const redirectUri = "http://localhost:4040/callback";
const logoutUri = "/api/logout";

export const requestLogin = () => {
    window.location = ("https://accounts.spotify.com/authorize?"+
        "response_type=code&client_id=e1d58f38936146dfb0775dcf231e0e75&redirect_uri="+
        encodeURIComponent(redirectUri)
    );
};

export const requestLogout = () => {
    return Axios.get(logoutUri)
        .then(()=> {
            return {
                isAuthenticated: false,
                authMessage: "Goodbye."
            };
        })
        .catch(() => {
            return {
                isAuthenticated: false,
                authMessage: "Server error, check your status please."
            };
        });
};