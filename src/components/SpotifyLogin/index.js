//import Axios from "axios";
import React from "react";
import Redirect from "react-router-dom";
import PropTypes from "prop-types";

import {requestLogin} from "./loginUtil";
import "./style.scss";

const Login = ({isAuthenticated, authMessage}) => {
    return (
        <section className="login-box">
            <h1>Iva&apos;s Spotify Playground</h1>
            {authMessage && (
                <div className="warning">{authMessage}</div>
            )}
            <button onClick={requestLogin}>Log in</button>
            {isAuthenticated && (
                <Redirect to={"/"}/>
            )}
        </section>
    );
};

Login.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    authMessage: PropTypes.string
};

export default Login;
