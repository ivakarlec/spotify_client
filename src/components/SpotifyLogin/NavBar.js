import React from "react";
import PropTypes from "prop-types";

import "./NavBar.scss";
import logo from "../../../assets/spotify_logo.svg";
import { requestLogout } from "./loginUtil";

const LoginNav = ({userName, onAuthChange}) => {
    const handleLogout = async () => {
        onAuthChange(await requestLogout());
    };
    return (
        <nav className="login">
            <img className="logo" src={logo} alt="spotify logo"/>
            <span>Hallo {userName}!</span>
            <button onClick={handleLogout}>logout</button>
        </nav>
    );
};

LoginNav.propTypes = {
    userName: PropTypes.string.isRequired,
    onAuthChange: PropTypes.func.isRequired
};

export default LoginNav;
