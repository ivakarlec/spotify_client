import React, {Component} from "react";
import PropTypes from "prop-types";

import "./InputGroup.scss";

class InputGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inputString: this.props.initialInput
        };
    }
    handleInputChange(e) {
        this.setState({inputString: e.target.value});
    }
    handleKeyPress(e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            this.props.handleSubmit(this.state.inputString);
            return false;
        } else {
            return true;
        }
    }
    handleSubmitButtonClick() {
        this.props.handleSubmit(this.state.inputString);
    }
    render() {
        return (
            <div className="input-group">
                <input
                    type="text"
                    value={this.state.inputString}
                    placeholder={this.props.placeholder}
                    onChange={this.handleInputChange.bind(this)}
                    onKeyPress={this.handleKeyPress.bind(this)}
                />
                <input
                    type="submit"
                    value={this.props.submitActionName}
                    onClick={ this.handleSubmitButtonClick.bind(this) }
                />
            </div>
        );
    }
}

InputGroup.defaultProps = {
    initialInput: "",
    placeholder: "",
    submitActionName: "submit"
};

InputGroup.propTypes = {
    initialInput: PropTypes.string,
    placeholder: PropTypes.string,
    submitActionName: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired
};

export default InputGroup;
