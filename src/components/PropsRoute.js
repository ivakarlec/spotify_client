import React from "react";
import {Route} from "react-router-dom";

const PropsRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={ () => (
            <div className="page-content">
                <Component {...rest} />
            </div>
        )} />
    );
};

export default PropsRoute;
