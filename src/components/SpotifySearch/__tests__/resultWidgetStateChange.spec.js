import { toggleFilter } from "../resultWidgetStateChange";

test("toggleFilter adds a new filter label at the beginning of the active filters array", () => {
    const startState = { activeFilters: ["albums", "artists"] };      
    const finState = toggleFilter(startState, "tracks");      
    expect(finState.activeFilters).toEqual(["tracks", "albums", "artists"]);
});

test("toggleFilter removes a filter label from filters array", () => {
    const startState = { activeFilters: ["tracks", "albums", "artists"] };      
    const finState = toggleFilter(startState, "albums");  
    expect(finState.activeFilters).toEqual(["tracks", "artists"]);
});