import Axios from "axios";

const searchPath = "/api/spotifySearch";

const throwIfNotOk = response => {
    if (response.status !== 200) {
        throw Error(response.statusText);
    } else if (response.data.hasOwnProperty("error")) {
        throw Error(response.data.error.message);
    }
    return response;
};

const sleep = (msecs) => (
    results => new Promise(resolve => setTimeout(() => resolve(results), msecs))
);

const getResults = response => response.data;

const loadData = (queryString) => {
    return Axios.post(searchPath, {q: queryString})
        .then(throwIfNotOk)
        .then(getResults)
        .then(sleep(1000));
};

export default loadData;