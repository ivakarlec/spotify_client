// trying out component folder pattern: https://medium.com/styled-components/component-folder-pattern-ee42df37ec68

import React, { Component } from "react";
import View from "./View";
import Loader from "../Loader";
import loadData from "./loadData";

const initialQuery = "";

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = { loading: !!initialQuery };
        this.load = this.load.bind(this);
    }

    async load(...args) {
        try {
            this.setState({ loading: true, error: "" });
            const data = await loadData(...args);
            const {isAuthenticated} = data;
            if (isAuthenticated !== undefined && !data.isAuthenticated) {
                this.props.onAuthChange({
                    isAuthenticated: false,
                    authMessage: "Your login has expired. You must login again."
                });
            } else {
                this.setState({ loading: false, error: "", data: data });
            }
        } catch (ex) {
            this.setState({ loading: false, error: ex.toString() });
        }
    }

    render() {
        return (
            <View {...this.props} {...this.state} initialQuery={initialQuery} onLoad={this.load} RenderLoading={Loader}/>
        );
    }
}