import React from "react";
import PropTypes from "prop-types";

import ResultThumbnail from "./ResultThumbnail.js";
import "./ResultList.scss";

const ResultList = ({data, showBigImage, resultType}) => {
    if(data === null || typeof data !== "object") {
        throw `Unexpected data structure for ${resultType} (${typeof data})`;
    }
    const renderTotalChunk = () => {
        return (data.total > data.limit ? data.limit : data.total)
            + " results of "
            + data.total
            + " ("+ (data.offset+1) + " - " + ( data.offset + data.items.length ) + ")";
    };
    const renderThumbnails = () => {
        let resultThumbnails = [];
        if(data.items.length > 0) {
            resultThumbnails = data.items.map((item, index ) => {
                return (
                    <ResultThumbnail
                        data={item}
                        showBigImage={(bigImage)=>{showBigImage(bigImage);}}
                        key={index}
                    />
                );
            });
        }
        return resultThumbnails;
    };
    return (
        <section>
            <div className="result-type-header">
                <h3>{resultType}</h3>
                <span>{renderTotalChunk()}</span>
            </div>
            <div className="result-list">
                {renderThumbnails()}
            </div>
        </section>
    );
};

ResultList.propTypes = {
    data: PropTypes.object.isRequired,
    showBigImage: PropTypes.func.isRequired,
    resultType: PropTypes.string.isRequired
};

export default ResultList;
