import React from "react";
import PropTypes from "prop-types";

const ResultThumbnail = ({data, showBigImage}) => {
    const getThumbInfo = () => {
        let infoString = "";
        switch (data.type) {
        case "album":
            infoString =  data.artists.map(artistsData => artistsData.name).join(", ");
            break;
        case "artist":
            infoString = data.genres.length>0 && "Genres: "+ data.genres.join(", ");
            break;
        case "playlist":
            infoString = "Tracks: "+ data.tracks.total;
            break;
        case "track":
            infoString = `Album: ${data.album.name} (${data.artists.map(artistsData => artistsData.name).join(", ")})`;
            break;
        }
        return infoString;
    };

    const getImage = (imgType) => {
        let imageData = null;
        if(data.images && data.images.length > 0) {
            switch (imgType) {
            case "thumb":
                imageData = data.images[data.images.length-1];
                break;
            case "big":
                imageData = data.images[0];
            }
        }
        return imageData;
    };

    const thumbInfo = getThumbInfo();
    const thumbImage = getImage("thumb");

    return (
        <figure>
            <div className="thumb-type">{data.type}</div>
            <p className="thumb-title">{data.name}</p>
            {   thumbImage && 
                <img 
                    onClick={() => {showBigImage(getImage("big"));}}
                    src={thumbImage.url} alt={data.name}
                    width={thumbImage.width}
                    height={thumbImage.height}/>
            }
            {   thumbInfo && 
                <div className="thumb-info">{thumbInfo}</div>
            }
        </figure>
    );
};

ResultThumbnail.propTypes = {
    data: PropTypes.object.isRequired,
    showBigImage: PropTypes.func.isRequired
};


export default ResultThumbnail;
