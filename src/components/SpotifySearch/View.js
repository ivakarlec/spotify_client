import React from "react";
import PropTypes from "prop-types";
import SearchInput from "../InputGroup.js";
import ResultWidget from "./ResultWidget.js";
import Loading from "./Loading.js";
import Error from "./Error.js";

const View = ({ 
    loading, error, data, initialQuery, onLoad,
    RenderSearchInput, RenderResult, RenderLoading, RenderError,
}) => (
    <div>
        <RenderSearchInput initialInput={initialQuery} handleSubmit={onLoad} />
        <section>
            { loading && <RenderLoading /> }
            { error && <RenderError error={error} /> }            
            { data && <RenderResult result={data} /> }
        </section>
    </div>
);

View.propTypes = {
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    src: PropTypes.string,
    initialQuery: PropTypes.string.isRequired,
    renderImage: PropTypes.func,
    renderLoading: PropTypes.func,
    renderError: PropTypes.func
};

View.defaultProps = {
    RenderSearchInput: SearchInput,
    RenderLoading: Loading,
    RenderResult: ResultWidget,
    RenderError: Error,
};

export default View;
