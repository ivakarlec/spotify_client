export const toggleFilter = (state, toggledLabel) => {
    const filters = state.activeFilters;
    const i = filters.indexOf(toggledLabel);
    let newFilterArr = [];
    if(i >= 0) {
        newFilterArr = filters.slice(0, i).concat(filters.slice(i+1));
    } else {
        newFilterArr = [toggledLabel].concat(filters);
    }
    return {
        ...state,
        activeFilters: newFilterArr
    };
};