import React from "react";
import PropTypes from "prop-types";

const LoadError = ({error}) => (
    <div className="errorText">
        { error }
    </div>
);

LoadError.propTypes = {
    error: PropTypes.string
};

export default LoadError;