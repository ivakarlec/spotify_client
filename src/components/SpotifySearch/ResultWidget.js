import React, {Component, Fragment} from "react";
import PropTypes from "prop-types";

import TypeFilter from "./TypeFilter";
import ResultList from "./ResultList";
import Modal from "../Modal";

import { toggleFilter } from "./resultWidgetStateChange";

class ResultWidget extends Component {
    constructor (props) {
        super(props);
        this.state = {
            activeFilters: [],
            modalOpen : false,
            bigImage : null,
            hasError: false
        };
    }
    componentDidCatch(error, info) {
        console.log(info);
        this.setState({hasError: true});
    }
    handleFilterChange(toggledLabel) {
        this.setState(toggleFilter(this.state, toggledLabel));
    }
    showBigImage (imageData) {
        this.setState({
            modalOpen: true,
            bigImage: imageData
        });
    }
    hideBigImage () {
        this.setState({
            modalOpen: false
        });
    }
    renderResultLists(result) {
        let activeFilters = this.state.activeFilters.length > 0 ? this.state.activeFilters : Object.keys(result);
        return activeFilters.map((resultType, index )=> {
            return <ResultList
                key={index}
                data={result[resultType]}
                resultType={resultType}
                showBigImage={(bigImage)=>{this.showBigImage(bigImage);}} />;
        });
    }
    render() {
        if(this.state.hasError) {
            return "Hopla, unexpected error occured. Try to search again.";
        } else {
            const { activeFilters, modalOpen, bigImage} = this.state;
            return (
                <Fragment>
                    <TypeFilter
                        key="type-filter"
                        labels={Object.keys(this.props.result)}
                        activeFilters={activeFilters}
                        handleFilterChange={(toggledLabel)=>this.handleFilterChange(toggledLabel)}
                    />
                    <p>Results:</p>
                    { this.renderResultLists(this.props.result) }
                    <Modal
                        key="big-image-modal"
                        contentClassName="full-screen-image"
                        show={ modalOpen }
                        onClose={ this.hideBigImage.bind(this) } >
                        { bigImage
                            && (<img src={bigImage.url}/>)}
                    </Modal>
                </Fragment>
            );
        }
    }
}

ResultWidget.propTypes = {
    result: PropTypes.objectOf(PropTypes.object).isRequired
};

export default ResultWidget;
