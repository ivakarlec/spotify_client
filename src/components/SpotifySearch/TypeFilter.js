import React, { Fragment  } from "react";
import PropTypes from "prop-types";

import "./TypeFilter.scss";

const TypeFilter = ({labels, activeFilters, handleFilterChange}) => {

    const TypeFilterList = labels.map( (label, index) => {
        return (
            <a key={index}
                onClick={() => handleFilterChange(label)}
                className={activeFilters.includes(label) ? "active" :""}
            >
                {label}
            </a>
        );
    });
    return (
        <Fragment>
            <div className="type-filter-hint">Filter result types:</div>
            <div className="type-filter">
                {TypeFilterList}
            </div>
        </Fragment>
    );
};

TypeFilter.propTypes = {
    labels: PropTypes.array.isRequired,
    activeFilters: PropTypes.array.isRequired,
    handleFilterChange: PropTypes.func.isRequired
};

export default TypeFilter;
