import React, {Component } from "react";
import { BrowserRouter as Router, Switch} from "react-router-dom";

import PrivateRoute from "./PrivateRoute";
import PropsRoute from "./PropsRoute";
import Login from "./SpotifyLogin";
import Search from "./SpotifySearch";

class App extends Component {
    constructor (props) {
        super(props);
        this.state = {
            loading: true,
            isAuthenticated: false,
            authMessage: "You have to login with your spotify account if you want to use Iva's playgroud."
        };
    }
    render() {
        return (
            <Router>
                <Switch>
                    <PrivateRoute exact path="/"
                        component={Search}
                        onAuthChange={(authState)=> {this.setState(authState);}}
                        {...this.state}
                    />
                    <PropsRoute
                        path="/login"
                        component={Login}
                        {...this.state}
                    />
                </Switch>
            </Router>
        );
    }
}

export default App;
