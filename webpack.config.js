const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

const VENDOR_LIBS = [
    "core-js", "react", "react-dom", "react-router-dom", "prop-types", "axios", "babel-polyfill"
];

module.exports = {
    entry: {
        bundle: "./src/index.js",
        vendor: VENDOR_LIBS
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: "[name].[hash:8].js",
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: { sourceMap: true }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true,
                            data:"@import 'variables';@import 'mixins';",
                            includePaths: [
                                path.join(__dirname, "src")
                            ]
                        }
                    }
                ],
                include: path.join(__dirname, "src")

            },
            {
                test: /\.(jpe?g|png|gif|svg|ico)$/i,
                use: [
                    "url-loader?limit=10000",
                    "img-loader"
                ]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "eslint-loader",
                enforce: "pre"
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(["dist"]),
        new webpack.HashedModuleIdsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            names: ["vendor", "manifest"]
        }),
        new HtmlWebpackPlugin({
            favicon: "assets/favicon.ico",
            template: "src/index.html",
            inject: true
        })
    ],
    devServer: {
        proxy: { // proxy URLs to backend development server
            "/api/*": "http://localhost:4040"
        },
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        contentBase: path.join(__dirname, "dist"), // boolean | string | array, static file location
        compress: true, // enable gzip compression
        historyApiFallback: true, // true for index.html upon 404, object for multiple paths
        https: false, // true for self-signed, object for cert authority
        noInfo: true, // only errors & warns on hot reload
    },
};
